<?php

/**
 * @file
 * Display Suite: Tweet Actions Template.
 *
 * Variables available:
 * - $tweet_id: the tweet identification number.
 * - $tweet_actions: the tweet actions that should be rendered.
 */

?>

<!-- Render the tweet button -->
<?php if ($tweet_actions['reply']): ?>
<a href="https://twitter.com/intent/tweet?in_reply_to=<?php print $tweet_id; ?>">Reply</a>
<?php endif; ?>
<!-- Render the retweet button -->
<?php if ($tweet_actions['retweet']): ?>
<a href="https://twitter.com/intent/retweet?tweet_id=<?php print $tweet_id; ?>">Retweet</a>
<?php endif; ?>
<!-- Render the favourite button -->
<?php if ($tweet_actions['favourite']): ?>
<a href="https://twitter.com/intent/favorite?tweet_id=<?php print $tweet_id; ?>">Favourite</a>
<?php endif; ?>
